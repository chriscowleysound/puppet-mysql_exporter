# mysql_exporter::service
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mysql_exporter::service
class mysql_exporter::service {
  include ::systemd
  file {'/etc/systemd/system/mysql_exporter.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('mysql_exporter/mysql_exporter.systemd.epp', {
      'bin_dir' => $::mysql_exporter::install_dir
      }
    )
  }
  ~>Exec['systemctl-daemon-reload']
  ->service {'mysql_exporter':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }
}
