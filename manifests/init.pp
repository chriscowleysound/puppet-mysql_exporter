# mysql_exporter
#
# Main class, includes all other classes
#
# @version [Optional[String]] Which version to install
# @manage_user [Optional[Boolean]] Whether to create the mysql_exporter user. Must be created by other means if set to false
# @manage_group [Optional[Boolean]] Whether to create the mysql_exporter group. Must be created by other means if set to false
# @archive_name [Optional[String]] URL of archive to download. 
#   include mysql_exporter
class mysql_exporter (
  $version = $::mysql_exporter::params::version,
  $manage_user = $::mysql_exporter::params::manage_user,
  $manage_group = $::mysql_exporter::params::manage_group,
  $archive_name = $::mysql_exporter::params::archive_name,
  $real_archive_url = $::mysql_exporter::params::real_archive_url,
  $local_archive_name = $::mysql_exporter::params::local_archive_name,

) inherits mysql_exporter::params {
  class { '::mysql_exporter::install': }
  ->class{ '::mysql_exporter::config': }
  ~>class{ '::mysql_exporter::service': }
  ->Class['::mysql_exporter']
}
