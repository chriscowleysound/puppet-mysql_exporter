# mysql_exporter::install
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mysql_exporter::install
class mysql_exporter::install {
  include ::archive

  if $mysql_exporter::manage_user {
    ensure_resource('user', ['mysql_exporter'], {
      ensure => 'present',
      system => true,
    })
  }
  if $mysql_exporter::manage_group {
    ensure_resource('group', ['mysql_exporter'], {
      ensure => 'present',
      system => true,
    })
    Group['mysql_exporter']->User['mysql_exporter']
  }
  archive { "/tmp/${mysql_exporter::local_archive_name}":
    ensure          => present,
    extract         => true,
    extract_path    => '/opt',
    source          => $mysql_exporter::real_archive_url,
    checksum_verify => false,
    creates         => "${::mysql_exporter::install_dir}/mysql_exporter",
    cleanup         => true,
  }
}
