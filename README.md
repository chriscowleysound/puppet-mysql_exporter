# mysql_exporter


#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with mysql_exporter](#setup)
    * [What mysql_exporter affects](#what-mysql_exporter-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with mysql_exporter](#beginning-with-mysql_exporter)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

Installs the Prometheus MySQL exporter

## Setup

### What mysql_exporter affects

Installs the release from Github and creates a systemd unit file to run the service under a dedicated user

### Beginning with mysql_exporter

```
node '<mysql-host>' {
  include ::mysql_exporter
}
```

## Limitations

Tested on CentOS 7 with Mariadb, but should work on any Linux that uses Systemd

## Development

Create a MR
